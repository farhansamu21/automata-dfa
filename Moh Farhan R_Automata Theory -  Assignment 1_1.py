# Color Initialization
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Initializing variables
w = []
x = ''
curr_state = 'q0'
final_state = ''

# Taking user input in decimal
print(bcolors.HEADER + bcolors.BOLD+ "\n\nL = {w | w \u2208{0,1}*:mod3} \n" + bcolors.ENDC)
user_input = input(bcolors.OKBLUE + "Enter your number (0_99) >> " + bcolors.ENDC)
print("\n")

# Checking every binary input
binary_arrays = list(bin(int(user_input)))

if (binary_arrays[2] == '0'):
    curr_state = 'q0'
else:
    curr_state = 'q1'

w.append(binary_arrays[2])
print("\u03B4((q0),"+''.join(w)+") = { " + curr_state + " } \n")

for binary in binary_arrays[3:]:

    pref_state = curr_state

    if (curr_state == 'q0'):
        if (binary == '0'):
            curr_state = 'q0'
        else:
            curr_state = 'q1'
    elif (curr_state == 'q1'):
        if (binary == '0'):
            curr_state = 'q2'
        else:
            curr_state = 'q0'
    elif (curr_state == 'q2'):
        if (binary == '0'):
            curr_state = 'q1'
        else:
            curr_state = 'q2'


    w.append(binary)
    print("\u03B4(q0," + ''.join(w) + ") = \u03B4(\u03B4(q0 , " + ''.join(w[::-1]) + "), " + binary + ") \n\t = \u03B4(" + pref_state + " , " + binary + ") \n\t = { "+curr_state+" }")  #Print the extended transaction function
    
# Print whether the input is rejected or accepted
if (curr_state == 'q0'):
    print(bcolors.FAIL + "\n REJECTED \n" + bcolors.ENDC)
else:
    print(bcolors.OKGREEN + "\n ACCEPTED \n" + bcolors.ENDC)    
