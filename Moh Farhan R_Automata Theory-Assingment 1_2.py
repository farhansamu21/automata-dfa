# Color Initialization
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

# Initializing variables
w = []
x = ''
curr_state = 'q0'
final_state = ''

# Taking user input in decimal
print(bcolors.HEADER + bcolors.BOLD+ "\n\nL = {w | w \u2208{a,b}*: every a immediatelly followed by b} \n" + bcolors.ENDC)
user_input = input(bcolors.OKBLUE + "Enter your array containing 'a' and 'b': " + bcolors.ENDC)
print("\n")

# Checking every binary input
binary_arrays = list(user_input) #make user input become an array

if (binary_arrays[0] == 'a'):
    curr_state = 'q1'
else:
    curr_state = 'q0'

w.append(binary_arrays[0])
print("\u03B4(q0,"+''.join(w)+") = { " + curr_state + " } \n")

for binary in binary_arrays[1:]:

    pref_state = curr_state

    # q0 condition
    if (curr_state == 'q0'):
        if (binary == 'a'):
            curr_state = 'q1'
        else:
            curr_state = 'q0'

    # q1 condition
    elif (curr_state == 'q1'):
        if (binary == 'a'):
            curr_state = 'q3'
        else:
            curr_state = 'q2'
    
    # q2 condition
    elif (curr_state == 'q2'):
        if (binary == 'a'):
            curr_state = 'q1'
        else:
            curr_state = 'q2'

    # q3 condition
    elif (curr_state == 'q3'):
        if (binary == 'a'):
            curr_state = 'q3'
        else:
            curr_state = 'q3'


    w.append(binary)
    print("\u03B4(q0," + ''.join(w) + ") = \u03B4(\u03B4(q0 , " + ''.join(w[::-1]) + "), " + binary + ") \n\t = \u03B4(" + pref_state + " , " + binary + ") \n\t = { "+curr_state+" }")  #Print the extended transaction function
    
    
    
# Print whether the input is rejected or accepted
if (curr_state == 'q1' or curr_state == 'q3'):
    print(bcolors.FAIL + "\n REJECTED \n" + bcolors.ENDC)
else:
    print(bcolors.OKGREEN + "\n ACCEPTED \n" + bcolors.ENDC)       
